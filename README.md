# ScrollRepeat

#### 介绍
unity中UGUI使用的无限滚动列表，继承ICanvasElement接口，可以正确识别ScrollRect的窗口大小。


#### 安装教程

方法一：拷贝Runtime下代码到工程
方法二：从PackageManager里引用

#### 使用说明
ScrollRepeat组件是一个无限滚动的列表，使用Vector2Int表示每个元素的坐标

ScrollRepeatListBase是一个有限数据的滚动列表，会将无限滚动的Vector2Int映射成index表示每个格子的坐标，这样方便获取格子对应的数据。
因此需要设置LimitRowCol，其x代表每一行有多少个，y代表一共有多少行。当值小于等于0时表示无限数量。
但x和y不能同时小于等于0，否则无法映射index
当x无限y有限时，元素顺序从上到下排序
当x有限y无限时，元素顺序从左到右排序
当x有限y有限，元素顺序从左到右排序

ScrollRepeatList是ScrollRepeatListBase对Tansform元素的扩展

ScrollRepeatListData是结合了元素和数据的基类，需要自己扩展