using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ViewListTestRender : MonoBehaviour, IScrollRepeatListRender<ListTestData>
{
    public TextMeshProUGUI txt;

    public void SetData(ListTestData v)
    {
        txt.text = v.index.ToString();
    }

    
}
