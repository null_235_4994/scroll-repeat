﻿using System.Collections.Generic;

public class ViewListTest : ScrollRepeatListData<ViewListTestRender, ListTestData>
{
    protected override void Start()
    {
        base.Start();

        List<ListTestData> list = new List<ListTestData>();

        for (int i = 0; i < 50; i++)
        {
            list.Add(new ListTestData() { index = i + 1 });
        }

        SetListData(list);
    }
}

public class ListTestData
{
    public int index;
}