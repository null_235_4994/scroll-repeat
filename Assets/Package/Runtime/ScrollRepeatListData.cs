using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public interface IScrollRepeatListRender<T>
{
    void SetData(T v);
}

[RequireComponent(typeof(ScrollRect))]
public class ScrollRepeatListData<TRender,TData> : ScrollRepeatListBase<TRender>, ICanvasElement where TRender : MonoBehaviour, IScrollRepeatListRender<TData>
{
    private List<TData> _listData;

    protected override void Start()
    {
        base.Start();
    }

    protected override void RenderChanged(TRender render, int index)
    {
        render.SetData(_listData[index]);
        base.RenderChanged(render, index);
    }

    public void SetListData(List<TData> v)
    {
        _listData = v;
        base.SetCount(_listData.Count);
    }

    public void SetListDataOrRefresh(List<TData> v)
    {
        _listData = v;
        base.SetCountOrRefresh(_listData.Count);
    }

    public override void SetCount(int v)
    {
        Debug.LogError("Use SetListData instead");
    }

    public override void SetCountOrRefresh(int v)
    {
        Debug.LogError("Use SetListDataOrRefresh instead");
    }
}