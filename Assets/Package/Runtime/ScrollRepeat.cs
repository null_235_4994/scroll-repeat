using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class ScrollRepeat <TRender> : UIBehaviour, ICanvasElement where TRender : MonoBehaviour
{
    private TRender render;

    private Dictionary<Vector2Int, TRender> _mapRenders;

    public Vector2 cellSize = new Vector2(100, 100);

    public Vector2 spacing;

    private RectInt _viewRect;

    private ScrollRect scrollRect;

    private Vector2 _viewSize;

    private Vector2 _itemSize;

    private List<TRender> _listPool;

    private Transform rootHide;

    public Action<TRender,Vector2Int> onRenderChanged;

    protected override void Awake()
    {
        base.Awake();

        scrollRect = GetComponent<ScrollRect>();
        scrollRect.content.anchorMin = new Vector2(0, 1);
        scrollRect.content.anchorMax = new Vector2(0, 1);
        scrollRect.content.anchoredPosition = new Vector2(0, 0);

        render = scrollRect.content.GetChild(0).GetComponent<TRender>();

        _mapRenders = new Dictionary<Vector2Int, TRender>();
        _listPool = new List<TRender>();

        GameObject obj = new GameObject("hide");
        obj.gameObject.SetActive(false);
        rootHide = obj.transform;
        rootHide.SetParent(scrollRect.content);
        render.transform.SetParent(rootHide);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);
    }

    protected override void Start()
    {
        base.Start();
        scrollRect.onValueChanged.AddListener(ContentScrollHandler);
    }

    public void LayoutComplete()
    {
        _viewSize = scrollRect.viewport.rect.size;
        _viewRect = new RectInt();

        _itemSize = cellSize + spacing;

        ContentScrollHandler(new Vector2());
    }

    private int GetRectXMin()
    {
        if (_itemSize.x == 0)
            return 0;
        return Mathf.FloorToInt(-scrollRect.content.anchoredPosition.x / _itemSize.x);
    }

    private int GetRectXMax()
    {
        if (_itemSize.x == 0)
            return 0;
        return Mathf.FloorToInt((-scrollRect.content.anchoredPosition.x + _viewSize.x) / _itemSize.x);
    }

    private int GetRectYMin()
    {
        if (_itemSize.y == 0)
            return 0;
        return Mathf.FloorToInt(scrollRect.content.anchoredPosition.y / _itemSize.y);
    }

    private int GetRectYMax()
    {
        if (_itemSize.y == 0)
            return 0;
        return Mathf.FloorToInt((scrollRect.content.anchoredPosition.y + _viewSize.y) / _itemSize.y);
    }

    private static List<Vector2Int> listTemp = new List<Vector2Int>();
    private void ContentScrollHandler(Vector2 normalizedPosition)
    {
        if (!Application.isPlaying)
            return;
        
        _viewRect.min = new Vector2Int(GetRectXMin(), GetRectYMin());        
        _viewRect.max = new Vector2Int(GetRectXMax(), GetRectYMax());

        listTemp.Clear();
        foreach (var item in _mapRenders)
        {
            if(_viewRect.Contains(item.Key) == false)
            {
                listTemp.Add(item.Key);
            }
        }
        for (int i = 0; i < listTemp.Count; i++)
        {
            RenderDestroy(_mapRenders[listTemp[i]]);
            _mapRenders.Remove(listTemp[i]);
        }
        listTemp.Clear();

        for (int y = _viewRect.min.y; y < _viewRect.max.y + 1; y++)
        {
            for (int x = _viewRect.min.x; x < _viewRect.max.x + 1; x++)
            {

                Vector2Int index = new Vector2Int(x, y);

                if (_mapRenders.ContainsKey(index) == false)
                {
                    TRender obj = RenderGet();

                    _mapRenders.Add(index, obj);

                    (_mapRenders[index].transform as RectTransform).anchoredPosition = new Vector2(index.x * _itemSize.x, -index.y * _itemSize.y);

                    RenderChanged(obj,index);
                }
            }
        }
    }

    protected virtual void RenderChanged(TRender obj,Vector2Int index)
    {
        if (onRenderChanged != null)
            onRenderChanged.Invoke(obj, index);
    }

    protected void RenderDestroy(TRender obj)
    {
        obj.transform.SetParent(rootHide);
        _listPool.Add(obj);
    }

    protected TRender RenderGet()
    {
        if(_listPool.Count > 0)
        {
            TRender obj = _listPool[0];
            _listPool.RemoveAt(0);
            obj.transform.SetParent(scrollRect.content);
            return obj;
        }

        return Instantiate(render, scrollRect.content);
    }

    public void Rebuild(CanvasUpdate executing)
    {
    }

    public void GraphicUpdateComplete()
    {
    }
}