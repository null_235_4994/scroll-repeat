using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;


[RequireComponent(typeof(ScrollRect))]
public class ScrollRepeatListBase<TRender> : UIBehaviour, ICanvasElement where TRender : Component
{
    private TRender render;

    private Dictionary<Vector2Int, TRender> _mapRenders;

    private Vector2 _cellSize;

    public Vector2 spacing;

    public float itemSizeX => _cellSize.x + spacing.x;
    public float itemSizeY => _cellSize.y + spacing.y;

    private Vector2 offset;

    private RectInt _viewRect;

    private ScrollRect scrollRect;

    private Vector2 _viewSize;

    private List<TRender> _listPool;

    private Transform rootHide;

    public UnityAction<TRender, int> onRenderChanged;

    private bool _inited;

    public Vector2Int limitRowCol;

    private int _itemCount;

    protected override void Awake()
    {
        base.Awake();

        scrollRect = GetComponent<ScrollRect>();
        scrollRect.content.anchorMin = new Vector2(0, 1);
        scrollRect.content.anchorMax = new Vector2(0, 1);
        scrollRect.content.anchoredPosition = new Vector2(0, 0);

        render = scrollRect.content.GetChild(0).GetComponent<TRender>();
        RectTransform renderTrabsfirn = render.GetComponent<RectTransform>();
        _cellSize = renderTrabsfirn.sizeDelta;
        renderTrabsfirn.anchorMin = new Vector2(0, 1);
        renderTrabsfirn.anchorMax = new Vector2(0, 1);
        offset = new Vector2(_cellSize.x * renderTrabsfirn.pivot.x, -_cellSize.y * renderTrabsfirn.pivot.y);


        _mapRenders = new Dictionary<Vector2Int, TRender>();
        _listPool = new List<TRender>();

        GameObject obj = new GameObject("hide");
        obj.gameObject.SetActive(false);
        rootHide = obj.transform;
        rootHide.SetParent(scrollRect.content);
        render.transform.SetParent(rootHide);
    }

    protected override void OnEnable()
    {
        base.OnEnable();
        CanvasUpdateRegistry.RegisterCanvasElementForLayoutRebuild(this);
    }

    protected override void OnDisable()
    {
        base.OnDisable();
        CanvasUpdateRegistry.UnRegisterCanvasElementForRebuild(this);
    }

    protected override void Start()
    {
        base.Start();
        scrollRect.onValueChanged.AddListener(ContentScrollHandler);
    }

    public void LayoutComplete()
    {
        _viewSize = scrollRect.viewport.rect.size;
        _viewRect = new RectInt();
        scrollRect.content.sizeDelta = new Vector2(GetContentSizeX(), GetContentSizeY());

        _inited = true;
        ContentScrollHandler(new Vector2());
    }

    private int GetRectXMin()
    {
        int x = Mathf.FloorToInt(-scrollRect.content.anchoredPosition.x / itemSizeX);
        if (x < 0)
            return 0;
        if (limitRowCol.x > 0 && x > limitRowCol.x)
            return limitRowCol.x;
        return x;
    }

    private int GetRectXMax()
    {
        int x = Mathf.CeilToInt((-scrollRect.content.anchoredPosition.x + _viewSize.x) / itemSizeX);
        if (x < 0)
            return 0;
        if (limitRowCol.x > 0 && x > limitRowCol.x)
            return limitRowCol.x;
        return x;
    }

    private int GetRectYMin()
    {
        int y = Mathf.FloorToInt(scrollRect.content.anchoredPosition.y / itemSizeY);
        if (y < 0)
            return 0;
        if (limitRowCol.y > 0 && y > limitRowCol.y)
            return limitRowCol.y;
        return y;
    }

    private int GetRectYMax()
    {
        int y = Mathf.CeilToInt((scrollRect.content.anchoredPosition.y + _viewSize.y) / itemSizeY);
        if (y < 0)
            return 0;
        if (limitRowCol.y > 0 && y > limitRowCol.y)
            return limitRowCol.y;
        return y;
    }

    private static List<Vector2Int> listTemp = new List<Vector2Int>();
    private void ContentScrollHandler(Vector2 normalizedPosition)
    {
        if (!Application.isPlaying)
            return;

        _viewRect.min = new Vector2Int(GetRectXMin(), GetRectYMin());
        _viewRect.max = new Vector2Int(GetRectXMax(), GetRectYMax());

        listTemp.Clear();
        foreach (var item in _mapRenders)
        {
            if (_viewRect.Contains(item.Key) == false)
            {
                Debug.Log($"Contains {item.Key} false");
                listTemp.Add(item.Key);
            }
            else
            {
                int index = GetIndex(item.Key.x, item.Key.y);
                if (index >= _itemCount || index < 0)
                {
                    Debug.Log($"{item.Key} index={index}  itemCount={_itemCount}");
                    listTemp.Add(item.Key);
                }
            }
        }

        for (int i = 0; i < listTemp.Count; i++)
        {
            RenderDestroy(_mapRenders[listTemp[i]]);
            _mapRenders.Remove(listTemp[i]);
        }
        listTemp.Clear();

        for (int y = _viewRect.min.y; y < _viewRect.max.y; y++)
        {
            for (int x = _viewRect.min.x; x < _viewRect.max.x; x++)
            {
                if (limitRowCol.x > 0 && x >= limitRowCol.x)
                    break;

                if (limitRowCol.y > 0 && y >= limitRowCol.y)
                    break;

                if (x < 0 || y < 0)
                    break;

                int index = GetIndex(x, y);

                if (index >= _itemCount)
                    break;

                Vector2Int vt2Index = new Vector2Int(x, y);
                if (_mapRenders.ContainsKey(vt2Index) == false)
                {
                    TRender obj = RenderGet();
                    obj.name = "Item_" + index;
                    _mapRenders.Add(vt2Index, obj);
                    (_mapRenders[vt2Index].transform as RectTransform).anchoredPosition = offset + new Vector2(vt2Index.x * itemSizeX, -vt2Index.y * itemSizeY);
                    RenderChanged(obj, index);
                }
            }
        }
    }

    private int GetIndex(int x,int y)
    {
        if (limitRowCol.x <= 0 && limitRowCol.y <= 0)
        {
            throw new NotImplementedException("limitRowCol X Y 不能同时<=0");
        }
        else if (limitRowCol.x <= 0 && limitRowCol.y > 0)
        {
            return x * limitRowCol.y + y;
        }

        return y * limitRowCol.x + x;
    }

    private void ReRender()
    {
        foreach (var item in _mapRenders)
        {
            int index = item.Key.x + item.Key.y * limitRowCol.x;
            if (limitRowCol.x <= 0 && limitRowCol.y > 0)
                index = item.Key.y + item.Key.x * limitRowCol.y;

            RenderChanged(item.Value, index);
        }
    }

    protected virtual void RenderChanged(TRender render, int index)
    {
        if (onRenderChanged != null)
            onRenderChanged.Invoke(render, index);
    }

    protected void RenderDestroy(TRender obj)
    {
        obj.transform.SetParent(rootHide);
        _listPool.Add(obj);
    }

    protected TRender RenderGet()
    {
        if (_listPool.Count > 0)
        {
            TRender obj = _listPool[0];
            _listPool.RemoveAt(0);
            obj.transform.SetParent(scrollRect.content);
            return obj;
        }
        return Instantiate(render, scrollRect.content);
    }

    public void Rebuild(CanvasUpdate executing)
    {

    }

    public void GraphicUpdateComplete()
    {

    }

    /// <summary>
    /// x>0时，从左到右横向布局
    /// x<=0，y>0时，从上到下竖向布局
    /// x<=0，y<=0时，从左到右横向布局，仅一行
    /// </summary>
    /// <returns></returns>
    private float GetContentSizeX()
    {
        if (limitRowCol.x > 0)
        {
            if(_itemCount < limitRowCol.x)
                return _itemCount * itemSizeX - (_itemCount <= 1 ? 0 : spacing.x);
            return limitRowCol.x * itemSizeX - (limitRowCol.x <= 1 ? 0 : spacing.x);
        }

        if(limitRowCol.y > 0)
        {
            int col = (_itemCount - 1) / limitRowCol.y + 1;
            return col * itemSizeX - (col <= 1 ? 0 : spacing.x);
        }

        return _itemCount * itemSizeX - (_itemCount <= 1 ? 0 : spacing.x);
    }

    /// <summary>
    /// x>0时，y默认等于0，每一行有x个，y轴无限滚动
    /// x<=0，y>0时，从上到下竖向布局 每一列y个，x轴无限滚动
    /// x<=0，y<=0时，从左到右横向布局，仅一行 y默认1
    /// </summary>
    /// <returns></returns>
    private float GetContentSizeY()
    {
        if(limitRowCol.x > 0)
        {
            int row = (_itemCount - 1) / limitRowCol.x + 1;
            return row * itemSizeY - (row <= 1 ? 0 : spacing.y);
        }

        if(limitRowCol.y > 0)
        {
            if (_itemCount < limitRowCol.y)
                return _itemCount * itemSizeY - (_itemCount <= 1 ? 0 : spacing.x);
            return limitRowCol.y * itemSizeY - (limitRowCol.y <= 1 ? 0 : spacing.x);
        }

        return _cellSize.y;
    }

    /// <summary>
    /// 设置数量，并重置位置
    /// </summary>
    /// <param name="v"></param>
    public virtual void SetCount(int v)
    {
        if (_inited)
        {
            _itemCount = v;
            scrollRect.normalizedPosition = Vector2.zero;
            //ContentScrollHandler(new Vector2());
        }
        else
        {
            _itemCount = v;
        }
    }

    /// <summary>
    /// 设置数量，数量相同就刷新
    /// </summary>
    /// <param name="v"></param>
    public virtual void SetCountOrRefresh(int v)
    {
        if(_inited)
        {
            if(_itemCount != v)
            {
                _itemCount = v;
                scrollRect.content.sizeDelta = new Vector2(GetContentSizeX(), GetContentSizeY());
                ContentScrollHandler(new Vector2());
            }
            else
            {
                ReRender();
            }
        }
        else
        {
            _itemCount = v;
        }
    }
}